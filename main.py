import os
import re

print("----------------WAN INFORMATION----------------")
def findPublicIp(pubIp= os.popen("wget -qO- icanhazip.com")):
    ip = ''.join(pubIp)
    print("PUBLIC IP:\n\t\t", ip)
    #Ip location
    command = f"whois {ip} |grep address".replace("\n", "")
    address = os.popen(command)
    addressIp = [i.replace("address","").replace(" ","").replace(":","").replace("\n","") for i in address]
    for i in range(len(addressIp)-1):
        for index, value in enumerate(addressIp):
            for v in addressIp[index+1:]:
                if value == v:
                    addressIp.remove(v)

    for i in addressIp:
        print("LOCATION:\n\t\t", i.replace(",",", "))



findPublicIp()

print("----------------LAN INFORMATION----------------")
def findPrivateIp(priIp= os.popen("ip addr show scope global | grep inet")):
    ipVFourFormat = "(([0-9]|[1-9][0-9]|1[0-9][0-9]|"\
                "2[0-4][0-9]|25[0-5])\\.){3}"\
                "([0-9]|[1-9][0-9]|1[0-9][0-9]|"\
                "2[0-4][0-9]|25[0-5])"
    p = re.compile(ipVFourFormat)
    for ip in priIp:
        res = re.search(p, ip)
        if res:
            print("PRIVATE IP:\n\t\t", ip[res.start():res.end()])
        else:
            print("check your connection!")

findPrivateIp()

print("----------------MAC INFORMATION----------------")
